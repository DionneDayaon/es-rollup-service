package com.godigital.esrollupservice.service;

import java.io.IOException;
import java.util.Collections;

import org.apache.http.HttpHost;
import org.elasticsearch.ElasticsearchStatusException;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.ShardSearchFailure;
import org.elasticsearch.client.HttpAsyncResponseConsumerFactory.HeapBufferedResponseConsumerFactory;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedStringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms.Bucket;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class RollupService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RollupService.class);

	@Value("${service.start.date}")
	private String initialStartDate;

	@Value("${elasticsearch.source.auth}")
	private String sourceCredentials;
	@Value("${elasticsearch.source.host}")
	private String sourceHost;
	@Value("${elasticsearch.source.port}")
	private Integer sourcePort;
	@Value("${elasticsearch.source.protocol}")
	private String sourceProtocol;
	@Value("${elasticsearch.source.index}")
	private String sourceIndex;

	@Value("${elasticsearch.target.auth}")
	private String targetCredentials;
	@Value("${elasticsearch.target.host}")
	private String targetHost;
	@Value("${elasticsearch.target.port}")
	private Integer targetPort;
	@Value("${elasticsearch.target.protocol}")
	private String targetProtocol;
	@Value("${elasticsearch.target.index}")
	private String targetIndex;

	public void rollupData(String dataType, String scriptName) {
		DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZZ").withZoneUTC();
		DateTime startDate = formatter.parseDateTime(initialStartDate);
		DateTime endDate = startDate.plusHours(1);
		startDate = startDate.plusMillis(1);

		RequestOptions sourceRequestOptions = getSourceRequestOptions(sourceCredentials);

		while (!startDate.isAfterNow()) {
			LOGGER.debug("Summarizing {} docs for period {} - {}", dataType, startDate, endDate);

			// QUERY
			QueryBuilder qb = null;
			if (dataType.equals("ads")) {
				qb = QueryBuilders.boolQuery()
						.filter(QueryBuilders.rangeQuery("@timestamp").gte(startDate).lte(endDate))
						.filter(QueryBuilders.termQuery("nimbusApp", "client"))
						.filter(QueryBuilders.existsQuery("ads"))
						.filter(QueryBuilders.existsQuery("calendar"));
			} else {
				qb = QueryBuilders.boolQuery()
						.filter(QueryBuilders.rangeQuery("@timestamp").gte(startDate).lte(endDate))
						.filter(QueryBuilders.termQuery("nimbusApp", "client"))
						.filter(QueryBuilders.existsQuery("appUsage"))
						.filter(QueryBuilders.existsQuery("calendar"));
			}

			// AGGREGATE
			Script script = new Script(ScriptType.STORED, null, scriptName, Collections.emptyMap());

			AggregationBuilder ag = AggregationBuilders.terms("distinct_message").script(script).size(6660); //(search.maxBuckets - 10) / 1.5 (https://stackoverflow.com/questions/57393548/control-number-of-buckets-created-in-an-aggregation)

			SearchRequest searchRequest = getSearchRequest(qb, ag);

			SearchResponse response = null;
			try (RestHighLevelClient client = new RestHighLevelClient(RestClient
					.builder(new HttpHost(sourceHost, sourcePort, sourceProtocol.isBlank() ? null : sourceProtocol)))) {
				response = client.search(searchRequest, sourceRequestOptions);
			} catch (Exception e) {
				LOGGER.error("", e);
				break;
			}

			if (response.getShardFailures().length > 0) {
				for (ShardSearchFailure failure : response.getShardFailures()) {
					LOGGER.error("{}", failure);
				}
				break;
			}

			ParsedStringTerms a = getTerms(response);

			try (RestHighLevelClient client = new RestHighLevelClient(RestClient
					.builder(new HttpHost(targetHost, targetPort, targetProtocol.isBlank() ? null : targetProtocol)))) {
				LOGGER.debug("will start saving results");
				for (Bucket b : a.getBuckets()) {
					String payload = modifyPayload(endDate, b);
					sendPayloadToTarget(sourceRequestOptions, client, payload);
				}
			} catch (Exception e) {
				LOGGER.error("", e);
				break;
			}

			LOGGER.debug("finished saving results");

			startDate = startDate.plusHours(1);
			endDate = endDate.plusHours(1);
			LOGGER.debug("Finished");
		}
	}

	private void sendPayloadToTarget(RequestOptions targetRequestOptions, RestHighLevelClient client, String payload) {
		try {
			IndexRequest ir = new IndexRequest(targetIndex, "_doc");
			ir.source(payload, XContentType.JSON);
			client.index(ir, targetRequestOptions);
		} catch (ElasticsearchStatusException | IOException e) {
			LOGGER.error("Error while saving payload {}", payload, e);
		}
	}

	private String modifyPayload(DateTime endDate, Bucket b) {
		StringBuilder stringBuilder = new StringBuilder(b.getKeyAsString());
		stringBuilder.insert(b.getKeyAsString().length() - 1,
				",\"count\":" + b.getDocCount() + ",\"@timestamp\": \"" + endDate.toString() + "\"");
		String payload = stringBuilder.toString();
//		LOGGER.debug(payload);
		return payload;
	}

	private ParsedStringTerms getTerms(SearchResponse response) {
		// distinct_message agg is from summarize-nimbus-usage script
		ParsedStringTerms a = (ParsedStringTerms) response.getAggregations().asMap().get("distinct_message");
		LOGGER.debug("Bucket count: {}", a.getBuckets().size());
		LOGGER.debug("Docs not included: {}", a.getSumOfOtherDocCounts());
		return a;
	}

	private SearchRequest getSearchRequest(QueryBuilder qb, AggregationBuilder ag) {
		SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
		searchSourceBuilder.query(qb);
		searchSourceBuilder.aggregation(ag);
		searchSourceBuilder.size(0);
		SearchRequest searchRequest = new SearchRequest(sourceIndex);
		searchRequest.source(searchSourceBuilder);
		return searchRequest;
	}

	private RequestOptions getSourceRequestOptions(String credentials) {
		RequestOptions.Builder builder = RequestOptions.DEFAULT.toBuilder();
		builder.addHeader("Authorization", credentials);
		builder.setHttpAsyncResponseConsumerFactory(HeapBufferedResponseConsumerFactory.DEFAULT);
		return builder.build();
	}

}
