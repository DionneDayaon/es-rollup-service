package com.godigital.esrollupservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.godigital.esrollupservice.service.RollupService;

@SpringBootApplication
public class EsRollupServiceApplication {
	
	public static void main(String[] args) {
		ConfigurableApplicationContext appContext = SpringApplication.run(EsRollupServiceApplication.class, args);
		
		appContext.getBean(RollupService.class).rollupData("ads", "summarize-nimbus-ads");
		appContext.getBean(RollupService.class).rollupData("usage", "summarize-nimbus-usage"); 
		
		appContext.close();
	}

}
